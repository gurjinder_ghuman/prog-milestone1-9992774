﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 1024;
            int b;
        
            Console.WriteLine("Please Enter The value of GB you wish to convert to MB");

            b = int.Parse(Console.ReadLine());
            Console.WriteLine($"The Answer is :");
            Console.WriteLine( (b * a));

            Console.WriteLine("Thankyou");
        }
    }
}
