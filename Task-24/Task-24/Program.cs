﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = 3;

            do
            {
                Console.Clear();

                Console.WriteLine($"%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                Console.WriteLine($"          MAIN MENU        ");
                Console.WriteLine($"        1. OPTION A        ");
                Console.WriteLine($"         2. OPTION B       ");
                Console.WriteLine($"                           ");
                Console.WriteLine($"%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                Console.WriteLine($"%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                Console.WriteLine();
                Console.WriteLine($"Please select an option");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();




                    Console.WriteLine($"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
                    Console.WriteLine($"          MENU 1              ");
                    Console.WriteLine($"                              ");
                    Console.WriteLine($"        1. OPTION B           ");
                    Console.WriteLine($"                              ");
                    Console.WriteLine($"        3. MAIN MENU          ");
                    Console.WriteLine($"                              ");
                    Console.WriteLine($"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine($"Please select an option");
                    menu = int.Parse(Console.ReadLine());


                }

                if (menu == 2)
                {
                    Console.Clear();

                    Console.WriteLine($"#############################");
                    Console.WriteLine($"             MENU 2          ");
                    Console.WriteLine($"                             ");
                    Console.WriteLine($"        a. OPTION 1          ");
                    Console.WriteLine($"                             ");
                    Console.WriteLine($"         3. MAIN MENU        ");
                    Console.WriteLine($"                             ");
                    Console.WriteLine($"#############################");
                    Console.WriteLine();
                    Console.WriteLine($"Please select an option");
                    var temp = Console.ReadLine();

                    if (temp == "3")
                    {
                        menu = int.Parse(temp);
                    }
                    else if (temp != "3")
                    {
                        if (temp == "a")
                        {
                            Console.Clear();
                            Console.WriteLine($"This is the result of option a/1");
                            Console.WriteLine($"Press <m> to go to the main menu.");
                            menu = int.Parse(Console.ReadLine());




                        }
                    }
                }


            } while (menu == 3);

            if (menu > 3)
            {
                Console.WriteLine($"Please choose the correct option");
                Console.WriteLine($"Press <3> to return to the main menu");
                Console.ReadLine();
                menu = 3;
                Console.Clear();

                



            }
        }
    }
}
