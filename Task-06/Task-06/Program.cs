﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, d, e;

            Console.WriteLine("Enter the First number");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the Second number");
            b = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the Third number");
            c = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the Fourth number");
            d = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the Fifth number");
            e = int.Parse(Console.ReadLine());

           

            Console.WriteLine($"Total Sum of Given Number is "+ (a + b + c + d + e));

            Console.WriteLine("Thankyou for using this Program");

            Console.ReadLine();
        }
    }
}
