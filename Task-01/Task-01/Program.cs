﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello There, How are you? Can I have your Name Please?");
            var name = Console.ReadLine();
            Console.WriteLine("And how old are you?");
            var age = 0;
            age = int.Parse(Console.ReadLine());

            Console.WriteLine("Your Name is " + name + " and You are " + age + " years old.");
            Console.WriteLine("Your Name is {0} and You are {1} years old.", name, age);
            Console.WriteLine($"Your Name is {name} and You are {age} years old.");


            Console.WriteLine("Thankyou for your Input");

        }
    }
}
