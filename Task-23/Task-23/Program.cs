﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var day = new Dictionary <string, string>();

            day.Add("monday", "weekday");
            day.Add("tuesday", "weekday");
            day.Add("wednesday", "weekday");
            day.Add("thursday", "weekday");
            day.Add("friday", "weekday");
            day.Add("saturday", "weekend");
            day.Add("sunday", "weekend");

            Console.WriteLine($"There are 7 days in a week");

            foreach (var x in day)
            {
                
                Console.WriteLine($"-{x.Key}");
            }

            

            var output = "";
            bool value = day.TryGetValue("weekday", out output);

            if (day.ContainsKey("monday"))
            {
                Console.WriteLine($"monday is a {day["monday"]}!");
            }

            var output1 = "";
            bool value1 = day.TryGetValue("weekday", out output);

            if (day.ContainsKey("tuesday"))
            {
                Console.WriteLine($"tuesday is a {day["tuesday"]}!");
            }

            var output2 = "";
            bool value2 = day.TryGetValue("weekday", out output);

            if (day.ContainsKey("wednesday"))
            {
                Console.WriteLine($"wednesday is a {day["wednesday"]}!");
            }

            var output3 = "";
            bool value3 = day.TryGetValue("weekday", out output);

            if (day.ContainsKey("thursday"))
            {
                Console.WriteLine($"thursday is a {day["thursday"]}!");
            }

            var output4 = "";
            bool value4 = day.TryGetValue("weekday", out output);

            if (day.ContainsKey("friday"))
            {
                Console.WriteLine($"friday is a {day["friday"]}!");
            }

            var output5 = "";
            bool value5 = day.TryGetValue("weekend", out output);

            if (day.ContainsKey("saturday"))
            {
                Console.WriteLine($"saturday and is a {day["saturday"]}!");
            }

            var output6 = "";
            bool value6 = day.TryGetValue("weekend", out output);

            if (day.ContainsKey("sunday"))
            {
                Console.WriteLine($"sunday is a {day["sunday"]}!");
            }

            // OR I guess I could write It Like This:
            
            var output8 = "";
            bool value8 = day.TryGetValue("weekday", out output);

            if (day.ContainsKey("monday"))
            {
                Console.WriteLine($"monday, tuesday, wednesday, thursday and friday is a {day["monday"]}!");
            }


        }
    }
}
