﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int>>();

            names.Add(Tuple.Create("JEFF", 30));
            names.Add(Tuple.Create("GURI", 26));
            names.Add(Tuple.Create("AKSHAY", 45));

            foreach (var x in names)
            {
                Console.WriteLine($"{x.Item1} is {x.Item2} old");
            }


        }
    }
}
