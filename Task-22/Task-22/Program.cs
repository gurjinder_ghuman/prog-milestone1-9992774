﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {

            var a = new Dictionary<string, string>();

            a.Add("mango", "fruit");
            a.Add("pear", "fruit");
            a.Add("apple", "fruit");
            a.Add("potato", "vege");
            a.Add("kumara", "vege");
            a.Add("carrot", "vege");



            foreach (var x in a)
            {
                Console.WriteLine($"-- {x.Key} is a {x.Value}--");
            }

            Console.WriteLine($"There are 3 Fruits and 3 Vegetables");


            var fruit = new string[3] { "mango", "pear", "apple" };

            var join = string.Join(", ", fruit);

            Console.WriteLine($"Below is the List of Fruits");
            
            Console.WriteLine(join);
           





        }
    }
}
