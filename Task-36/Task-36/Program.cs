﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 3;
            var i = 0;

            if (counter == i)
            {
                Console.WriteLine("Values are equal");
            }
            else
            {
                Console.WriteLine("Counter Value is not equal to index");
            }

            
            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
                Console.WriteLine();
                
            }
            Console.WriteLine($"We manage to print these line because counter has bigger number than index.!");
        }
    }
}
