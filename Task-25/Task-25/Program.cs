﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Type in a number and press <ENTER>");

            
            try
            {
                 
                var input = int.Parse(Console.ReadLine());
                Console.WriteLine($"{input} x 2 = {input * 2}");
            }
            catch (Exception e)
            {
                
                Console.WriteLine(e);
            }

            
            Console.WriteLine("Type in a number and press <ENTER>");

            
            var input1 = Console.ReadLine(); 
            var a = 0; 
            bool value = int.TryParse(input1, out a); 

            
            Console.WriteLine($"You typed in: {input1}");
            Console.WriteLine($"Did you enter in a number? : {value}");


            Console.WriteLine($"Thankyou for using this program!");
        }
    }
}
