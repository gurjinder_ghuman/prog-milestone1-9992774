﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Please Enter 3 numbers and we will answer them with different math Equations!");

            var a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());
            var c = int.Parse(Console.ReadLine());

            Console.WriteLine($"Q1: | {a}+{b}*{c} ="); int.Parse(Console.ReadLine());
            Console.WriteLine($"Q2: | {a}*{b}+{c} ="); int.Parse(Console.ReadLine());
            Console.WriteLine($"Q3: | {a}-{b}*{c} ="); int.Parse(Console.ReadLine());
            Console.WriteLine($"Q4: | {a}+{b}/{c} ="); int.Parse(Console.ReadLine());
            Console.WriteLine($"Q5: | {a}/{b}+{c} ="); int.Parse(Console.ReadLine());
            Console.ReadLine();

            Console.WriteLine($"Correct Answer are Listed bellow ! ");
            


            Console.WriteLine($"Q1: {a}+{b}*{c} = {(a + b) * (c)}");
            Console.WriteLine($"Q1: {a}*{b}+{c} = {(a * b) + (c)}");
            Console.WriteLine($"Q1: {a}-{b}*{c} = {(a - b) * (c)}");
            Console.WriteLine($"Q1: {a}+{b}/{c} = {(a + b) / (c)}");
            Console.WriteLine($"Q1: {a}/{b}+{c} = {(a / b) + (c)}");

            Console.WriteLine($"If your Answer are not Correct, TRY AGAIN! Practice makes Perfect :)");
        }
    }
}
